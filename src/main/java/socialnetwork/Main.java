package socialnetwork;

import socialnetwork.controller.FriendshipControllerInterface;
import socialnetwork.controller.UserControllerInterface;
import socialnetwork.controller.implementation.FriendshipController;
import socialnetwork.controller.implementation.UserController;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.FriendshipValidator;
import socialnetwork.domain.validators.UserValidator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.FriendshipDatabaseRepository;
import socialnetwork.repository.database.UserDatabaseRepository;
import socialnetwork.service.FriendshipServiceInterface;
import socialnetwork.service.UserServiceInterface;
import socialnetwork.service.implementation.FriendshipService;
import socialnetwork.service.implementation.UserService;
import socialnetwork.userinterface.UserInterface;


public class Main {

    private static final String URL = "jdbc:postgresql://localhost:5432/social_network";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "Diamondfarm21";

    public static void main(String[] args) {
        Repository<Long, User> userRepository = new UserDatabaseRepository(new UserValidator(), URL, USERNAME,
                PASSWORD);
        FriendshipDatabaseRepository friendshipDatabaseRepository = new FriendshipDatabaseRepository(new
                FriendshipValidator(), URL, USERNAME, PASSWORD, userRepository);
        UserServiceInterface userService = new UserService(userRepository, friendshipDatabaseRepository);
        FriendshipServiceInterface friendshipService = new FriendshipService(userRepository,
                friendshipDatabaseRepository);
        UserControllerInterface userController = new UserController(userService);
        FriendshipControllerInterface friendshipController = new FriendshipController(friendshipService);
        UserInterface userInterface = new UserInterface(userController, friendshipController);
        userInterface.run();
    }
}