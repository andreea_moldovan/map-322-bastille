package socialnetwork.controller.implementation;

import socialnetwork.controller.FriendshipControllerInterface;
import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.IllegalFriendshipException;
import socialnetwork.service.FriendshipServiceInterface;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class FriendshipController implements FriendshipControllerInterface {
    private final FriendshipServiceInterface friendshipService;

    public FriendshipController(FriendshipServiceInterface friendshipService) {
        this.friendshipService = friendshipService;
    }

    @Override
    public Friendship saveFriendship(Friendship friendship) throws IllegalFriendshipException {
        return friendshipService.saveFriendship(friendship);
    }

    @Override
    public Friendship deleteFriendship(Tuple<Long, Long> id) throws IllegalFriendshipException, IllegalArgumentException {
        return friendshipService.deleteFriendship(id);
    }

    @Override
    public Iterable<Friendship> findAll() {
        return friendshipService.findAll();
    }

    @Override
    public Friendship getFriendshipByID(Tuple<Long, Long> id) {
        return friendshipService.getFriendshipByID(id);
    }

    @Override
    public Map<Long, List<Long>> loadUsersAndFriends() {
        return friendshipService.loadUsersAndFriends();
    }

    @Override
    public int countTheNumberOfCommunitiesInTheNetwork() {
        return friendshipService.countTheNumberOfCommunitiesInTheNetwork();
    }

    @Override
    public HashSet<Integer> getTheMostNumerousCommunityOfFriends() {
        return friendshipService.getTheMostNumerousCommunityOfFriends();
    }
}
