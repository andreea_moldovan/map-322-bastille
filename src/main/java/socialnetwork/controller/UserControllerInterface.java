package socialnetwork.controller;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.IllegalFriendshipException;

public interface UserControllerInterface {
    User saveUser(User user) throws IllegalFriendshipException;

    User deleteUser(Long id) throws IllegalFriendshipException;

    User updateUser(User user);

    Iterable<User> findAll();

    User getUserByID(Long id);
}
