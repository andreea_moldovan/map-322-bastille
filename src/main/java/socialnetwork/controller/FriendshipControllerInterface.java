package socialnetwork.controller;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.IllegalFriendshipException;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

public interface FriendshipControllerInterface {
    Friendship saveFriendship(Friendship friendship) throws IllegalFriendshipException;

    Friendship deleteFriendship(Tuple<Long, Long> id) throws IllegalFriendshipException, IllegalArgumentException;

    Iterable<Friendship> findAll();

    Friendship getFriendshipByID(Tuple<Long, Long> id);

    Map<Long, List<Long>> loadUsersAndFriends();

    int countTheNumberOfCommunitiesInTheNetwork();

    HashSet<Integer> getTheMostNumerousCommunityOfFriends();
}
