package socialnetwork.service.implementation;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.IllegalFriendshipException;
import socialnetwork.repository.Repository;
import socialnetwork.service.UserServiceInterface;

import java.util.ConcurrentModificationException;


public class UserService implements UserServiceInterface {
    private final Repository<Long, User> userRepository;
    private final Repository<Tuple<Long, Long>, Friendship> friendshipRepository;

    public UserService(Repository<Long, User> userRepository, Repository<Tuple<Long, Long>,
            Friendship> friendshipRepository) {
        this.userRepository = userRepository;
        this.friendshipRepository = friendshipRepository;
    }

    @Override
    public User saveUser(User user) throws IllegalFriendshipException {
        return userRepository.save(user);
    }

    @Override
    public User updateUser(User user) {
        return userRepository.update(user);
    }

    @Override
    public User deleteUser(Long id) throws IllegalFriendshipException, ConcurrentModificationException {
        for (Friendship friendship : friendshipRepository.findAll()) {
            if (friendship.getId().getId1().equals(id) || friendship.getId().getId2().equals(id)) {
                friendshipRepository.delete(friendship.getId());
            }
        }
        return userRepository.delete(id);
    }

    @Override
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User getUserByID(Long id) {
        return userRepository.findOne(id);
    }
}