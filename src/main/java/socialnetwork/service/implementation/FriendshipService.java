package socialnetwork.service.implementation;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.User;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.graph.Graph;
import socialnetwork.domain.validators.IllegalFriendshipException;
import socialnetwork.repository.Repository;
import socialnetwork.service.FriendshipServiceInterface;

import java.util.*;

public class FriendshipService implements FriendshipServiceInterface {
    private final Repository<Long, User> userRepository;
    private final Repository<Tuple<Long, Long>, Friendship> friendshipRepository;

    public FriendshipService(Repository<Long, User> userRepository, Repository<Tuple<Long, Long>, Friendship>
            friendshipRepository) {
        this.userRepository = userRepository;
        this.friendshipRepository = friendshipRepository;
    }

    public Friendship saveFriendship(Friendship friendship) throws IllegalFriendshipException,
            IllegalArgumentException {
        return friendshipRepository.save(friendship);
    }

    @Override
    public Friendship deleteFriendship(Tuple<Long, Long> id) throws IllegalFriendshipException,
            ConcurrentModificationException {
        return friendshipRepository.delete(id);
    }

    @Override
    public Iterable<Friendship> findAll() {
        return friendshipRepository.findAll();
    }

    @Override
    public Friendship getFriendshipByID(Tuple<Long, Long> id) {
        return friendshipRepository.findOne(id);
    }

    public Map<Long, List<Long>> loadUsersAndFriends() {
        Map<Long, List<Long>> userAndFriends = new HashMap<>();
        for (User user : userRepository.findAll()) {
            userAndFriends.put(user.getId(), new ArrayList<>());
        }
        for (Friendship friendship : friendshipRepository.findAll()) {
            userAndFriends.get(friendship.getId().getId1()).add(userRepository.
                    findOne(friendship.getId().getId2()).getId());
            userAndFriends.get(friendship.getId().getId2()).add(userRepository.
                    findOne(friendship.getId().getId1()).getId());
        }
        return userAndFriends;
    }

    @Override
    public Graph generateGraphOfCommunities() {
        Graph network = new Graph(userRepository.size());
        List<Integer> listOfIDs = new ArrayList<>();
        int index = 0, index1, index2;
        for (Friendship p : friendshipRepository.findAll()) {
            int firstVertex = p.getId().getId1().intValue();
            int secondVertex = p.getId().getId2().intValue();
            if (!listOfIDs.contains(firstVertex)) {
                index1 = index;
                index++;
                listOfIDs.add(firstVertex);
            } else {
                index1 = listOfIDs.lastIndexOf(firstVertex);
            }
            if (!listOfIDs.contains(secondVertex)) {
                index2 = index;
                index++;
                listOfIDs.add(secondVertex);
            } else {
                index2 = listOfIDs.lastIndexOf(secondVertex);
            }
            network.addEdge(index1, index2);
        }
        return network;
    }

    @Override
    public int countTheNumberOfCommunitiesInTheNetwork() {
        Graph network = generateGraphOfCommunities();
        return network.countTheNumberOfCommunitiesOfFriends();
    }

    @Override
    public HashSet<Integer> getTheMostNumerousCommunityOfFriends() {
        Graph network = generateGraphOfCommunities();
        return network.getTheMostNumerousCommunityOfFriends();
    }
}
