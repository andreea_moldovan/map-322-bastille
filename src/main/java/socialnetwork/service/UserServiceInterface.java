package socialnetwork.service;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.IllegalFriendshipException;

public interface UserServiceInterface {
    User saveUser(User user) throws IllegalFriendshipException;

    User updateUser(User user);

    User deleteUser(Long id) throws IllegalFriendshipException;

    Iterable<User> findAll();

    User getUserByID(Long id);
}
