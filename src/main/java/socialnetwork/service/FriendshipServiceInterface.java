package socialnetwork.service;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.graph.Graph;
import socialnetwork.domain.validators.IllegalFriendshipException;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

public interface FriendshipServiceInterface {
    Friendship saveFriendship(Friendship friendship) throws IllegalFriendshipException;

    Friendship deleteFriendship(Tuple<Long, Long> id) throws IllegalFriendshipException;

    Iterable<Friendship> findAll();

    Friendship getFriendshipByID(Tuple<Long, Long> id);

    Graph generateGraphOfCommunities();

    Map<Long, List<Long>> loadUsersAndFriends();

    int countTheNumberOfCommunitiesInTheNetwork();

    HashSet<Integer> getTheMostNumerousCommunityOfFriends();
}
