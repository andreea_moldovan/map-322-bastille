package socialnetwork.userinterface;

import socialnetwork.controller.FriendshipControllerInterface;
import socialnetwork.controller.UserControllerInterface;
import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.IllegalFriendshipException;
import socialnetwork.utils.IdGenerator;

import java.time.LocalDate;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static socialnetwork.utils.DateFormatter.*;

public class UserInterface {
    private static final String OPTION1 = "1";
    private static final String OPTION2 = "2";
    private static final String OPTION3 = "3";
    private static final String OPTION4 = "4";
    private static final String OPTION5 = "5";
    private static final String OPTION6 = "6";
    private static final String OPTION7 = "7";
    private static final String OPTION8 = "8";
    private static final String OPTION9 = "9";
    private static final String OPTION10 = "10";
    private static final String OPTION11 = "11";
    private static final String EXIT = "x";

    private final UserControllerInterface userController;
    private final FriendshipControllerInterface friendshipController;

    public UserInterface(UserControllerInterface userController, FriendshipControllerInterface friendshipController) {
        this.userController = userController;
        this.friendshipController = friendshipController;
    }

    private static void showMenu() {
        System.out.println("1.  ADD AN USER");
        System.out.println("2.  DELETE AN USER");
        System.out.println("3.  UPDATE AN USER");
        System.out.println("4.  SHOW ALL USERS");
        System.out.println("5.  GET AN USER");
        System.out.println("6.  ADD A FRIENDSHIP");
        System.out.println("7.  DELETE A FRIENDSHIP");
        System.out.println("8.  SHOW ALL FRIENDSHIPS");
        System.out.println("9.  GET A FRIENDSHIP");
        System.out.println("10.  DISPLAY THE NUMBER OF COMMUNITIES OF FRIENDS");
        System.out.println("11. DISPLAY THE MOST NUMEROUS COMMUNITY OF FRIENDS");
        System.out.println("x. EXIT");
    }

    private void saveUser() {
        System.out.println("Give the first name of the user: ");
        Scanner scannerFirstName = new Scanner(System.in);
        String firstName = scannerFirstName.nextLine();
        System.out.println("Give the last name of the user: ");
        Scanner scannerLastName = new Scanner(System.in);
        String lastName = scannerLastName.nextLine();
        try {
            User user = new User(firstName, lastName);
            user.setId(IdGenerator.generateID());
            userController.saveUser(user);
        } catch (IllegalArgumentException | IllegalFriendshipException exception) {
            System.out.println(exception.getMessage());
        }
    }

    private void updateUser() {
        System.out.println("Give the ID of the user to be updated: ");
        Scanner scannerID = new Scanner(System.in);
        Long id = scannerID.nextLong();
        System.out.println("Give the first name of the user: ");
        Scanner scannerFirstName = new Scanner(System.in);
        String firstName = scannerFirstName.nextLine();
        System.out.println("Give the last name of the user: ");
        Scanner scannerLastName = new Scanner(System.in);
        String lastName = scannerLastName.nextLine();
        try {
            User user = new User(firstName, lastName);
            user.setId(id);
            userController.updateUser(user);
        } catch (IllegalArgumentException exception) {
            System.out.println(exception.getMessage());
        }
    }

    private void deleteUser() {
        System.out.println("Give the ID of the user to be removed: ");
        Scanner scannerID = new Scanner(System.in);
        Long id = scannerID.nextLong();
        try {
            userController.deleteUser(id);
        } catch (IllegalArgumentException | IllegalFriendshipException exception) {
            System.out.println(exception.getMessage());
        }
    }

    public void findAllUsers() {
        System.out.println("The users are: ");
        for (Map.Entry<Long, List<Long>> pair : friendshipController.loadUsersAndFriends().entrySet()) {
            System.out.println(userController.getUserByID(pair.getKey()));
            System.out.println("FRIENDS:");
            if (!pair.getValue().isEmpty()) {
                pair.getValue().forEach(ID -> System.out.println(userController.getUserByID(ID)));
            } else {
                System.out.println("NONE");
            }
            System.out.println();
        }
    }

    private void getAnUserByID() {
        System.out.println("Give the ID of the user: ");
        Scanner scannerID1 = new Scanner(System.in);
        Long id = scannerID1.nextLong();
        if (userController.getUserByID(id) != null) {
            System.out.println(userController.getUserByID(id));
        } else {
            System.out.println("No user with this ID");
        }
    }

    private void displayTheNumberOfCommunitiesOfFriends() {
        System.out.println("The number of communities is: " + friendshipController.
                countTheNumberOfCommunitiesInTheNetwork());
    }

    public void saveFriendship() {
        System.out.println("Give the ID of the first friend: ");
        Scanner scannerID1 = new Scanner(System.in);
        Long id1 = scannerID1.nextLong();
        System.out.println("Give the ID of the second friend: ");
        Scanner scannerID2 = new Scanner(System.in);
        Long id2 = scannerID2.nextLong();
        System.out.println("Enter the date with the next format yyyy-MM-dd: ");
        Scanner scannerDate = new Scanner(System.in);
        String datetime = scannerDate.nextLine();
        LocalDate date = geFormattedStringDate(datetime);
        Friendship friendship = new Friendship(id1, id2, date);
        try {
            friendshipController.saveFriendship(friendship);
        } catch (IllegalFriendshipException exception) {
            System.out.println(exception.getMessage());
        }
    }

    private void deleteFriendship() {
        System.out.println("Give the ID of the first friend: ");
        Scanner scannerID1 = new Scanner(System.in);
        Long id1 = scannerID1.nextLong();
        System.out.println("Give the ID of the second friend: ");
        Scanner scannerID2 = new Scanner(System.in);
        Long id2 = scannerID2.nextLong();
        try {
            friendshipController.deleteFriendship(new Tuple<>(id1, id2));
        } catch (IllegalArgumentException | IllegalFriendshipException exception) {
            System.out.println(exception.getMessage());
        }
    }

    private void findAllFriendships() {
        System.out.println("The friendships are: ");
        friendshipController.findAll().forEach(System.out::println);
    }

    private void getAFriendshipByID() {
        System.out.println("Give the ID of the first friend: ");
        Scanner scannerID1 = new Scanner(System.in);
        Long id1 = scannerID1.nextLong();
        System.out.println("Give the ID of the second friend: ");
        Scanner scannerID2 = new Scanner(System.in);
        Long id2 = scannerID2.nextLong();
        Tuple<Long, Long> id = new Tuple<>(id1, id2);
        if (friendshipController.getFriendshipByID(id) != null) {
            System.out.println(friendshipController.getFriendshipByID(id));
        } else {
            System.out.println("No friendship with this ID");
        }
    }

    private void displayTheMostNumerousCommunityOfFriends() {
        System.out.println("The number of users belonging to the most numerous community of friends is: ");
        System.out.println(friendshipController.getTheMostNumerousCommunityOfFriends().size());
    }

    public void run() {
        String option;
        label:
        while (true) {
            showMenu();
            System.out.println("Select the option: ");
            Scanner scanner = new Scanner(System.in);
            option = scanner.nextLine();
            switch (option) {
                case OPTION1:
                    saveUser();
                    break;
                case OPTION2:
                    deleteUser();
                    break;
                case OPTION3:
                    updateUser();
                    break;
                case OPTION4:
                    findAllUsers();
                    break;
                case OPTION5:
                    getAnUserByID();
                    break;
                case OPTION6:
                    saveFriendship();
                    break;
                case OPTION7:
                    deleteFriendship();
                    break;
                case OPTION8:
                    findAllFriendships();
                    break;
                case OPTION9:
                    getAFriendshipByID();
                    break;
                case OPTION10:
                    displayTheNumberOfCommunitiesOfFriends();
                    break;
                case OPTION11:
                    displayTheMostNumerousCommunityOfFriends();
                    break;
                case EXIT:
                    break label;
                default:
                    System.out.println("The selected option is invalid!");
                    break;
            }
        }
    }
}
