package socialnetwork.repository.database;

import socialnetwork.domain.User;
import socialnetwork.domain.validators.IllegalFriendshipException;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class UserDatabaseRepository extends AbstractDatabaseRepository<Long, User> {

    private static final String INSERT = "insert into users (first_name, last_name ) values (?, ?)";
    private static final String DELETE = "delete from users where id = ?";
    private static final String SELECT = "select * from users;";
    private static final String UPDATE = "update users set first_name = ?, last_name = ?  where id=?";
    private static final String FIND_ONE = "SELECT * from users where id = ?";
    private static final String ID = "id";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";

    public UserDatabaseRepository(Validator<User> validator, String url, String username, String password) {
        super(validator, url, username, password);
    }

    @Override
    public User save(User user) throws IllegalArgumentException, IllegalFriendshipException {
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(INSERT)) {
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return super.save(user);
    }

    @Override
    public User delete(Long id) throws IllegalFriendshipException {
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(DELETE)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return super.delete(id);
    }

    @Override
    public User findOne(Long userID) {
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(FIND_ONE)) {
            statement.setLong(1, userID);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return getUser(userID, resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return super.findOne(userID);
    }

    @Override
    public Iterable<User> findAll() {
        Set<User> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(SELECT);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long id = resultSet.getLong(ID);
                User user = getUser(id, resultSet);
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return super.findAll();
    }

    @Override
    public User update(User user) {
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setLong(3, user.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return super.update(user);
    }

    private User getUser(Long userID, ResultSet resultSet) throws SQLException {
        String firstName = resultSet.getString(FIRST_NAME);
        String lastName = resultSet.getString(LAST_NAME);
        User user = new User(firstName, lastName);
        user.setId(userID);
        return user;
    }
}
