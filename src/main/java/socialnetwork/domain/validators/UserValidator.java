package socialnetwork.domain.validators;


import socialnetwork.domain.User;

public class UserValidator implements Validator<User> {
    @Override
    public void validate(User user) throws ValidationException {
        StringBuilder errors = new StringBuilder();
        if (user.getLastName().isEmpty() || user.getLastName() == null) {
            errors.append("Invalid last name\n");
        }
        if (user.getFirstName().isEmpty() || user.getFirstName() == null) {
            errors.append("Invalid first name");
        }
        if (!errors.isEmpty()) {
            throw new ValidationException(errors.toString());
        }
    }
}
