package socialnetwork.domain.validators;

public class IllegalFriendshipException extends Exception {
    public IllegalFriendshipException(String message) {
        super(message);
    }
}
