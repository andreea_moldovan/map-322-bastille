package socialnetwork.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateFormatter {
    private static final String FORMAT = "yyyy-MM-dd";

    public static LocalDate geFormattedStringDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT);
        return LocalDate.parse(date, formatter);
    }

    public static String getFormattedLocalDate(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT);
        return date.format(formatter);
    }
}

